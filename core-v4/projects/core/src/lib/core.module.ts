import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { AppConfig } from './common/config/app-config';
import { CustomConfig } from './common/config/custom-config';
import { ConfigFactory } from './common/config/config-factory';
import { BrowserWindowService } from './services/browser-window.service';
import { LocalStorageService } from './services/local-storage.service';
import { CommonDateYearComponent } from './components/common-date-year';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  declarations: [
    CommonDateYearComponent,
  ],
  exports: [
    CommonDateYearComponent,
  ]
})
export class NgCoreCoreModule {
  static forRoot(): ModuleWithProviders<NgCoreCoreModule> {
    return {
      ngModule: NgCoreCoreModule,
      providers: [
        AppConfig,
        // CustomConfig,
        ConfigFactory,
        BrowserWindowService,
        LocalStorageService
      ]
    };
  }
}
