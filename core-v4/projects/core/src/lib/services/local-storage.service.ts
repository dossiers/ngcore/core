import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import { DevLogger as dl } from '../common/logging/dev-logger'; import isDL = dl.isLoggable;


/**
 * Utility functions related to browser localStorage object.
 */
@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor( @Inject(PLATFORM_ID) protected platformId: Object) {
  }

  get hasStorage(): boolean {
    return !!this.localStorage;
  }

  /**
   * Returns browser localStorage object.
   * Returns null if the localStorage object is not available or not accessible.
   */
  get localStorage(): (Storage | null) {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage;
    } else {
      // if(isDL()) dl.log("localStorage not available.");
      return null;  // ??
    }
  }

  get length(): number {
    if (this.localStorage) {
      return this.localStorage.length;
    } else {
      // if(isDL()) dl.log("get length ignored.");
      return 0;
    }
  }

  key(index: number): (string | null) {
    if (this.localStorage) {
      return this.localStorage.key(index);
    } else {
      // if(isDL()) dl.log(`key() ignored. index = ${index}`);
      return null;
    }
  }

  getItem(key: string): (string | null) {
    if (this.localStorage) {
      try {
        return this.localStorage.getItem(key);
      } catch (ex) {
        if(isDL()) dl.log(`getItem() failed. key = ${key}; ex = ${ex}`);
        return null;
      }
    } else {
      // if(isDL()) dl.log(`getItem() ignored. key = ${key}`);
      return null;
    }
  }

  get(key: string): (any | null) {
    let value = this.getItem(key);
    return value && JSON.parse(value);
  }

  setItem(key: string, value: string): void {
    if (this.localStorage) {
      try {
        this.localStorage.setItem(key, value);
      } catch (ex) {
        if(isDL()) dl.log(`setItem() failed. key = ${key}; value = ${value}; ex = ${ex}`);
      }
    } else {
      // if(isDL()) dl.log(`setItem() ignored. key = ${key}; value = ${value}`);
    }
  }

  set(key: string, value: any): void {
    try {
      this.setItem(key, JSON.stringify(value));
    } catch (ex) {
      if(isDL()) dl.log(`set() failed. key = ${key}; value = ${value}; ex = ${ex}`);
    }
  }

  removeItem(key: string): void {
    if (this.localStorage) {
      try {
        this.localStorage.removeItem(key);
      } catch (ex) {
        if(isDL()) dl.log(`removeItem() failed. key = ${key}; ex = ${ex}`);
      }
    } else {
      // if(isDL()) dl.log(`removeItem() ignored. key = ${key}`);
    }
  }

  remove(key: string): void {
    this.removeItem(key);
  }

  clear(): void {
    if (this.localStorage) {
      try {
        this.localStorage.clear();
      } catch (ex) {
        if(isDL()) dl.log(`clear() failed. ex = ${ex}`);
      }
    } else {
      // if(isDL()) dl.log(`clear() ignored.`);
    }
  }

}
