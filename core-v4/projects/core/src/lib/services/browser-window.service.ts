import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, DOCUMENT } from '@angular/common';


/**
 * Utility functions related to Web browser window/document objects.
 */
@Injectable({
  providedIn: 'root'
})
export class BrowserWindowService {

  constructor(
    @Inject(PLATFORM_ID) protected platformId: Object,
    @Inject(DOCUMENT) private windowDocument
  ) {
  }

  /**
   * Returns browser window object if the app is running in the browser.
   * Returns null otherwise.
   */
  public get window(): (Window | null) {
    if (isPlatformBrowser(this.platformId)) {
      return window;
    } else {
      return null;  // ???
    }
  }

  /**
   * Returns browser document object if the app is running in the browser.
   * Returns null otherwise.
   */
  public get document(): (any | null) {
    if (isPlatformBrowser(this.platformId)) {
      // return this.window.document;
      return this.windowDocument;
    } else {
      return null;  // ???
    }
  }

}
