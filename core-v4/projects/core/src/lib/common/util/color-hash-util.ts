import { RGBColor } from '../core/rgb-color';
import { ColorHash } from '../core/color-hash';
import { RGBColorUtil } from './rgb-color-util';

// Color hash is a list of 9 colors (arranged in a square).
export namespace ColorHashUtil {

  export function randomColorHash() : ColorHash {
    let colors: RGBColor[] = [
      RGBColorUtil.randomColor(), RGBColorUtil.randomColor(), RGBColorUtil.randomColor(),
      RGBColorUtil.randomColor(), RGBColorUtil.randomColor(), RGBColorUtil.randomColor(),
      RGBColorUtil.randomColor(), RGBColorUtil.randomColor(), RGBColorUtil.randomColor()
    ];
    let hash = new ColorHash(colors);
    return hash;
  }

}
