import { DevLogger as dl } from '../logging/dev-logger'; import isDL = dl.isLoggable;


/**
 * Time-related utility functions.
 */
export namespace DateTimeUtil {

  export function getUnixEpochMillis(date: Date = new Date()): number {
    return date.getTime();
  }

  // ISO 8601 foramt
  export function getISODateTimeString(now: number = getUnixEpochMillis(), excludeTimePart: boolean = false): string {
    let dt = new Date(now);
    return getISODateString(dt, excludeTimePart);
  }
  export function getISODateString(date: Date, excludeTimePart: boolean = false): string {
    let str: string = "";
    // temporary
    // let locale = "en-us";
    let locale = undefined;
    str = date.toISOString();
    if(excludeTimePart) {
      str = str.substring(0, 10);
    }
  // if(isDL()) dl.log(">>>>>>> date = " + date + "; str = " + str);
    return str;
  }

  // TBD:
  // Returns the "hour" (in our usage) of a given time.
  export function getHourOfTheDay(date: Date = new Date()): number {
    // tbd:
    let h = date.getHours();
    let m = date.getMinutes();
    let s = date.getSeconds();
    let n = date.getMilliseconds();

    let hour = h + m * (1 / 60) + s * (1 / 3600) + n * (1 / 3600000);
    // tbd: format it (e.g., down to 2 decimals below zero) ????
    return hour;
  }

  export function getYear(date: Date = new Date()): number {
    let y = date.getFullYear();
    return y;
  }


  export function getMidnightDate(date: Date = new Date()): Date {
    let dt = new Date(date);
    dt.setHours(0, 0, 0, 0);
    // dt.setUTCHours(0,0,0,0);
    return dt;
  }
  export function getMidnight(now: number = getUnixEpochMillis()): number {
    let dt = new Date(now);
    dt.setHours(0, 0, 0, 0);
    // dt.setUTCHours(0,0,0,0);
    let midnight = getUnixEpochMillis(dt);
    return midnight;
  }

  // Sunday is considered the first day of a week.
  export function getSundayMidnightDate(date: Date = new Date()): Date {
    let dt = new Date(date);
    dt.setHours(0, 0, 0, 0);
    let delta = dt.getDay();
    dt.setDate(dt.getDate() - delta);
    return dt;
  }
  export function getSundayMidnight(now: number = getUnixEpochMillis()): number {
    let dt = new Date(now);
    dt.setHours(0, 0, 0, 0);
    let delta = dt.getDay();
    dt.setDate(dt.getDate() - delta);
    let sundayMidnight = getUnixEpochMillis(dt);
    return sundayMidnight;
  }

  // The first day of a month.
  export function getFirstDayMidnightDate(date: Date = new Date()): Date {
    let dt = new Date(date);
    dt.setHours(0, 0, 0, 0);
    dt.setDate(1);
    return dt;
  }
  export function getFirstDayMidnight(now: number = getUnixEpochMillis()): number {
    let dt = new Date(now);
    dt.setHours(0, 0, 0, 0);
    dt.setDate(1);
    let firstMidnight = getUnixEpochMillis(dt);
    return firstMidnight;
  }

  /**
   * Returns the number of the day in the given month.
   * 
   * @param today {Date} A date in the given month. 
   */
  export function getNumberOfDaysInMonth(today: Date = new Date()): number {
    let mo = today.getMonth() + 1;

    let numDays = 0;
    switch (mo) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        numDays = 31;
        break;
      case 4:
      case 6:
      case 9:
      case 11:
      default:   // ???
        numDays = 30;
        break;
      case 2:
        numDays = 28;
        let year = today.getFullYear();
        if (((year % 4) === 0) && ((year % 100) !== 0)) {
          numDays = 29;
        }
        break;
    }
    return numDays;
  }

  export function getNumberOfDaysForMonth(now: number = getUnixEpochMillis()): number {
    let dt = new Date(now);
    dt.setDate(1);
    return getNumberOfDaysInMonth(dt);
  }
  export function getNumberOfDaysForPreviousMonth(now: number = getUnixEpochMillis()): number {
    let dt = new Date(now);
    dt.setDate(1);
    dt.setMonth(dt.getMonth() - 1);
    return getNumberOfDaysInMonth(dt);
  }
  export function getNumberOfDaysForNextMonth(now: number = getUnixEpochMillis()): number {
    let dt = new Date(now);
    dt.setDate(1);
    dt.setMonth(dt.getMonth() + 1);
    return getNumberOfDaysInMonth(dt);
  }

}
