import { DateTimeUtil } from './date-time-util';
import { DateIdUtil } from './date-id-util';


/**
 * Util functions for generating unique IDs.
 */
export namespace UniqueIdUtil {

  // temporary implementation.
  // The current implementation guarantees neither uniqueness nor monotonicity.
  // --> TBD: need a better implementation.

  // the primary purpose of this method is to provide a client-side id 
  //   for entities to be stored in DBs like CouchDB, for instance. 
  // The advantage of this implementation (say, over a random/uuid-based id)
  //   is the ids are (more or less) ordered (in time).
  //   (It is also a disadvantage at the same time 
  //    since this is not good as PK/Hash for most NoSQL databases.
  //    --> In such a case, use "random id" instead.)
  // (TBD: Add a runtime counter, to differentiate events within the same millisecond?
  //    It has to monotonically increase, not just cyclic, over time.)
  export function id(): string {
    // // Use decimal
    // let millis = (DateTimeUtil.getUnixEpochMillis() % 1000000000000);  // Remove the leading 1. 
    // let rand = Math.floor(Math.random() * 1000);
    // // We'll end up with 15 digit numbers.  e.g.,   100 000 000 000 000
    // let uid = millis * 1000 + rand; 
    // // return uid.toString();
    // // Or, We'll end up with 16 digit string.  e.g.,   100 000 000 000 - 000
    // // let strid = millis + '-' + rand; 
    // let str = uid.toString();
    // let strid = str.substring(0, str.length - 3) + '-' + str.substr(str.length - 3);
    // return strid;

    // Use hexadecimal
    let millis = (DateTimeUtil.getUnixEpochMillis() % 1000000000000);  // Remove the leading 1. 
    let rand = Math.floor(Math.random() * 100);
    let uid = millis * 100 + rand;  // We'll end up with 14 digit numbers.  e.g.,   500 000 000 000 rr
    // Convert to hex.
    let strid = uid.toString(16);   // We will end up with 12-digit hex number.
    return strid;
  }

}
