/**
 * Util functions for random number generation.
 */
export namespace RandomNumberUtil {

  export function randomIndex(list: any[]) : number {
    if(! list) {
      return -1;
    }
    let len = list.length;
    let idx = Math.floor(Math.random() * len);
    return idx;
  }

  export function randomElement(list: any[]) : any {
    if(! list) {
      return null;
    }
    let len = list.length;
    let idx = Math.floor(Math.random() * len);
    return list[idx];
  }


}
