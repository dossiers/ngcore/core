import { DevLogger as dl } from '../logging/dev-logger'; import isDL = dl.isLoggable;
import { RGBColor } from './rgb-color';


/**
 * Color hash is a list of 9 colors (arranged in a square).
 */
export class ColorHash {

  // TBD:
  // Color management should be really done in Lab Delta E, not in RGB....

  // 9 RGB numbers [0, 255]
  constructor(public colors: (RGBColor[] | null) = null) {
    // if (this.colors) {
    //   if(isDL()) dl.log('>>> ColorHash.colors = ' + this.colors.toString());
    // } else {
    //   if(isDL()) dl.log('>>> ColorHash.colors = null.');
    // }
  }


  // TBD.
  // equals(obj: ColorHash): boolean {
  //   return true;
  // }

  // clone?
  

  // temporary
  toString(): string {
    let str = '%';
    if (this.colors) {
      // in vs of.
      for (let c of this.colors) {
        str += c.toString();
      }
      // let len = this.colors.length;
      // for (let i = 0; i < len; i++) {
      //   let c = this.colors[i];
      //   if(c) {
      //      str += c.toString();
      //   }
      // }
    }
    str += '%';
    return str;
  }

}
