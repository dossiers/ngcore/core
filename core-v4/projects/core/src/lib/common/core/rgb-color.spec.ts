import { async, inject, TestBed } from '@angular/core/testing';
import {} from 'jasmine';

import { RGBColor } from './rgb-color';


// https://angular.io/docs/ts/latest/guide/testing.html

describe('RGBColor class', () => {
  let rgbColor: RGBColor;
  let r: number, g: number, b: number;
  let str: string;

  beforeEach(async(() => {
    r = 0xff;
    g = 0xa0;
    b = 0;
    str = "#ffa000";

    rgbColor = new RGBColor(r, g, b);
  }));

  it ('should be propertly contructed', () => {
    expect(typeof rgbColor).not.toBeNull();
  });

  it ('properties should work properly', () => {
    expect(rgbColor.R).toBe(r);
    expect(rgbColor.G).toBe(g);
    expect(rgbColor.B).toBe(b);
  });

  it ('should have toString()', () => {
    expect(rgbColor.toString()).toBe(str);
  });

});
