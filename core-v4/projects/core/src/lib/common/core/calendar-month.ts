/**
 * Months: January (1) through December (12).
 * (This is useful in EN locale only.)
 */
export enum CalendarMonth {
  unknown = 0,
  january = 1,
  february = 2,
  march = 3,
  april = 4,
  may = 5,
  june = 6,
  july = 7,
  august = 8,
  september = 9,
  october = 10,
  november = 11,
  december = 12
}
