import { async, inject, TestBed } from '@angular/core/testing';
// import { IonicModule } from 'ionic-angular';
import {} from 'jasmine';

import { RGBColor } from './rgb-color';
import { ColorHash } from './color-hash';


// https://angular.io/docs/ts/latest/guide/testing.html

describe('ColorHash class', () => {
  let color1 = new RGBColor(0x11, 0x22, 0x33);
  let color2 = new RGBColor(0x55, 0x66, 0x77);
  let color3 = new RGBColor(0xaa, 0xbb, 0xcc);
  let colorHash: ColorHash;
  let str: string;

  beforeEach(async(() => {
    colorHash = new ColorHash([color1, color2, color3]);
    str = "%#112233#556677#aabbcc%";
  }));

  it ('should be propertly contructed', () => {
    expect(typeof colorHash).not.toBeNull();
  });

  it ('properties should work properly', () => {
    expect(colorHash.colors).not.toBeNull();
  });

  it ('should have toString()', () => {
    expect(colorHash.toString()).toBe(str);
  });


});
