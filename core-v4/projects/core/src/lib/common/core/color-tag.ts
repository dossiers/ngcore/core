import { RGBColor } from './rgb-color';
import { GrayscaleColor } from './grayscale-color';
import { ColorPalette } from './color-palette';
import { ColorHash } from './color-hash';


/**
 * Color tag is a list of 9 colors from ColorPalette.
 */
export class ColorTag extends ColorHash {

  // temporary
  // (The index list will change if color-palette changes.)
  static BW_CHECKER_BOARD_A: ColorTag = new ColorTag([0, 1, 0, 1, 0, 1, 0, 1, 0]);
  static BW_CHECKER_BOARD_B: ColorTag = new ColorTag([1, 0, 1, 0, 1, 0, 1, 0, 1]);
  static BW_CHECKER_BOARD_C: ColorTag = new ColorTag([1, 2, 1, 2, 1, 2, 1, 2, 1]);
  static BW_CHECKER_BOARD_D: ColorTag = new ColorTag([2, 1, 2, 1, 2, 1, 2, 1, 2]);
  static BW_CHECKER_BOARD_E: ColorTag = new ColorTag([2, 3, 2, 3, 2, 3, 2, 3, 2]);
  static BW_CHECKER_BOARD_F: ColorTag = new ColorTag([3, 2, 3, 2, 3, 2, 3, 2, 3]);

  static BW_ALLWHITE_BOARD: ColorTag = new ColorTag([0, 0, 0, 0, 0, 0, 0, 0, 0]);
  static BW_ALLSILVER_BOARD: ColorTag = new ColorTag([1, 1, 1, 1, 1, 1, 1, 1, 1]);
  static BW_ALLGRAY_BOARD: ColorTag = new ColorTag([2, 2, 2, 2, 2, 2, 2, 2, 2]);

  static BW_SILVER_SQUARE: ColorTag = new ColorTag([1, 1, 1, 1, 0, 1, 1, 1, 1]);
  static BW_GRAY_SQUARE: ColorTag = new ColorTag([2, 2, 2, 2, 1, 2, 2, 2, 2]);
  static BW_DARKGRAY_SQUARE: ColorTag = new ColorTag([3, 3, 3, 3, 2, 3, 3, 3, 3]);

  static BW_SILVER_DOT: ColorTag = new ColorTag([0, 0, 0, 0, 1, 0, 0, 0, 0]);
  static BW_GRAY_DOT: ColorTag = new ColorTag([1, 1, 1, 1, 2, 1, 1, 1, 1]);
  static BW_DARKGRAY_DOT: ColorTag = new ColorTag([2, 2, 2, 2, 3, 2, 2, 2, 2]);


  // tbd:
  static fromString(str: string): ColorTag {
    let indexes: (number[] | null) = null;
    if (str) {
      indexes = str.split('-').map((item) => {
        return parseInt(item, 10);
      });
    }
    let tag = new ColorTag(indexes);
    return tag;
  }

  // tbd
  // grayscaleColors: GrayscaleColor[];
  private _grayscaleColors: (GrayscaleColor[] | null) = null;
  get grayscaleColors(): GrayscaleColor[] {
    if(this._grayscaleColors == null) {
      if (this.indexes != null) {  // ????
        this._grayscaleColors = ColorPalette.convertToGrayscaleColors(this.indexes);
      } else {
        // this._grayscaleColors = null;   // ??
        this._grayscaleColors = [];   // ??
      }
    }
    return this._grayscaleColors;
  }


  // TBD: Default constructor???

  // 9 color indexes from ColorPalette.
  // tbd: validate?
  // tbd: indexes should not be assignable... How to remove setter?
  constructor(public indexes: (number[] | null)) {
    super((indexes != null && indexes.length >= 5)   // ???
      ? ColorPalette.convertToColors(indexes) : null);
    // if (indexes != null && indexes.length >= 5) {  // ????
    //   this.grayscaleColors = ColorPalette.convertToGrayscaleColors(indexes);
    // } else {
    //   this.grayscaleColors = null;   // ??
    // }
  }


  equals(obj: ColorTag): boolean {
    if (!obj) return false;
    if (this.indexes == null && obj.indexes == null) return true;   // ????
    if (this.indexes == null || obj.indexes == null) return false;
    if (this.indexes.length !== obj.indexes.length) return false;
    for (let i in this.indexes) {
      if (this.indexes[i] !== obj.indexes[i]) return false;
    }
    return true;
  }


  // temporary
  toString(): string {
    // return super.toString();

    // let str = '';
    // for(let idx of this.indexes) {
    //   str += (idx < 10) ? '0' + idx : idx;
    // }

    let str = (this.indexes != null) ? this.indexes.join('-') : '';
    return str;
  }

  clone(): ColorTag {
    let cloned = Object.assign(new ColorTag(this.indexes), this) as ColorTag;
    // grayscaleColors ??
    return cloned;
  }
  static clone(obj: any): ColorTag {
    let cloned = Object.assign(new ColorTag(obj.indexes), obj) as ColorTag;   // ????
    // grayscaleColors ??
    return cloned;
  }

}
