import { async, inject, TestBed } from '@angular/core/testing';
// import { IonicModule } from 'ionic-angular';
import {} from 'jasmine';

import { RGBColor } from './rgb-color';
import { ColorPalette } from './color-palette';
import { ColorHash } from './color-hash';
import { ColorTag } from './color-tag';


// https://angular.io/docs/ts/latest/guide/testing.html

describe('ColorTag class', () => {
  let colorTag: ColorTag;
  let str: string;

  beforeEach(async(() => {
    colorTag = new ColorTag([0,1,2,  3,4,5,  6,7,8]);
    str = "0-1-2-3-4-5-6-7-8";
  }));

  it ('should be propertly contructed', () => {
    expect(typeof colorTag).not.toBeNull();
  });

  it ('properties should work properly', () => {
    expect(colorTag.colors).not.toBeNull();
  });

  it ('should have toString()', () => {
    expect(colorTag.toString()).toBe(str);
  });


});

describe('ColorTag fromString()', () => {
  let tag: ColorTag;
  let str: string;

  beforeEach(async(() => {
    tag = new ColorTag([0,1,2,  3,4,5,  6,7,8]);
    str = "0-1-2-3-4-5-6-7-8";
  }));

  it ('should work propertly.', () => {
    let colorTag = ColorTag.fromString(str);
    expect(typeof colorTag).not.toBeNull();
  });

  it ('properties should work properly', () => {
    let colorTag = ColorTag.fromString(str);
    expect(colorTag.equals(tag)).toBeTruthy();
  });

  it ('.toString() should return the original string', () => {
    let colorTag = ColorTag.fromString(str);
    expect(colorTag.toString()).toBe(str);
  });


});


describe('ColorTag clone()', () => {
  let tag: ColorTag;
  let str: string;

  beforeEach(async(() => {
    tag = new ColorTag([0,1,2,  3,4,5,  6,7,8]);
    str = tag.toString();
  }));

  it ('should work propertly.', () => {
    let colorTag = tag.clone();;
    expect(typeof colorTag).not.toBeNull();
  });

  it ('should create an identical object', () => {
    let colorTag = tag.clone();;
    expect(colorTag.equals(tag)).toBeTruthy();
  });

  it ('.toString() should return the original string', () => {
    let colorTag = tag.clone();;
    expect(colorTag.toString()).toBe(str);
  });


});
