/**
 * Array of 3 numbers (color components).
 */
export class RGBColor {

  public static readonly WHITE : RGBColor = new RGBColor(255, 255, 255);
  public static readonly BLACK : RGBColor = new RGBColor(0, 0, 0);

  // 3 numbers [0, 255]: R, G, B.
  // constructor(public R: number = 0, public G: number = 0, public B: number = 0) {
  constructor(public R: number, public G: number, public B: number) {
  }


  equals(obj: RGBColor): boolean {
    if(!obj)  {
      return false;
    }
    if(this.R === obj.R && this.G === obj.G && this.B === obj.B) {
      return true;
    } else {
      return false;
    }
  }

  // Format: "#ffggbb".
  toString(): string {
    // return '(' + this.R + ',' + this.G + ',' + this.B + ')';

    let r = this.R.toString(16);
    let g = this.G.toString(16);
    let b = this.B.toString(16);

    let str = '#';
    str += (r.length < 2) ? '0' + r : r;
    str += (g.length < 2) ? '0' + g : g;
    str += (b.length < 2) ? '0' + b : b;
    // str += 'ff';  // alpha.

    return str;
  }

}
