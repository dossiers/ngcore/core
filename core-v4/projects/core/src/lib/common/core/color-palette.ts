import { DevLogger as dl } from '../logging/dev-logger'; import isDL = dl.isLoggable;
import { RGBColor } from './rgb-color';
import { GrayscaleColor } from './grayscale-color';


/**
 * Defines a set of colors.
 */
export namespace ColorPalette {
  export const Color00 = new RGBColor(0xff, 0xff, 0xff);
  export const GrayscaleColor00 = GrayscaleColor.convertToGrayscale(Color00);

  // export const Color01 = new RGBColor(0xd9, 0, 0);
  // export const Color02 = new RGBColor(0, 0xd9, 0);
  // export const Color03 = new RGBColor(0, 0, 0xd9);
  // export const Color04 = new RGBColor(0xf5, 0xf5, 0);
  // export const Color05 = new RGBColor(0xf5, 0, 0xf5);
  // export const Color06 = new RGBColor(0, 0xf5, 0xf5);
  // export const Color07 = new RGBColor(0xd0, 0xd0, 0xd0);    // silver
  // export const Color08 = new RGBColor(0x77, 0x77, 0x77);    // gray

  export const Color01 = new RGBColor(0xff, 0, 0);
  export const GrayscaleColor01 = GrayscaleColor.convertToGrayscale(Color01);

  export const Color02 = new RGBColor(0, 0xff, 0);
  export const GrayscaleColor02 = GrayscaleColor.convertToGrayscale(Color02);

  export const Color03 = new RGBColor(0, 0, 0xff);
  export const GrayscaleColor03 = GrayscaleColor.convertToGrayscale(Color03);

  export const Color04 = new RGBColor(0x90, 0, 0);
  export const GrayscaleColor04 = GrayscaleColor.convertToGrayscale(Color04);

  export const Color05 = new RGBColor(0, 0x90, 0);
  export const GrayscaleColor05 = GrayscaleColor.convertToGrayscale(Color05);

  export const Color06 = new RGBColor(0, 0, 0x90);
  export const GrayscaleColor06 = GrayscaleColor.convertToGrayscale(Color06);


  export const Color07 = new RGBColor(0xff, 0xff, 0);
  export const GrayscaleColor07 = GrayscaleColor.convertToGrayscale(Color07);

  export const Color08 = new RGBColor(0xff, 0, 0xff);
  export const GrayscaleColor08 = GrayscaleColor.convertToGrayscale(Color08);

  export const Color09 = new RGBColor(0, 0xff, 0xff);
  export const GrayscaleColor09 = GrayscaleColor.convertToGrayscale(Color09);

  export const Color10 = new RGBColor(0xa0, 0xa0, 0);
  export const GrayscaleColor10 = GrayscaleColor.convertToGrayscale(Color10);

  export const Color11 = new RGBColor(0xa0, 0, 0xa0);
  export const GrayscaleColor11 = GrayscaleColor.convertToGrayscale(Color11);

  export const Color12 = new RGBColor(0, 0xa0, 0xa0);
  export const GrayscaleColor12 = GrayscaleColor.convertToGrayscale(Color12);


  export const Color13 = new RGBColor(0xf0, 0x50, 0);
  export const GrayscaleColor13 = GrayscaleColor.convertToGrayscale(Color13);

  export const Color14 = new RGBColor(0x50, 0, 0xf0);
  export const GrayscaleColor14 = GrayscaleColor.convertToGrayscale(Color14);

  export const Color15 = new RGBColor(0, 0xf0, 0x50);
  export const GrayscaleColor15 = GrayscaleColor.convertToGrayscale(Color15);

  export const Color16 = new RGBColor(0x50, 0xf0, 0);
  export const GrayscaleColor16 = GrayscaleColor.convertToGrayscale(Color16);

  export const Color17 = new RGBColor(0xf0, 0, 0x50);
  export const GrayscaleColor17 = GrayscaleColor.convertToGrayscale(Color17);

  export const Color18 = new RGBColor(0, 0x50, 0xf0);
  export const GrayscaleColor18 = GrayscaleColor.convertToGrayscale(Color18);


  export const Color90 = new RGBColor(0xf0, 0xf0, 0xf0);    // almost white
  export const GrayscaleColor90 = GrayscaleColor.convertToGrayscale(Color90);

  export const Color91 = new RGBColor(0xd0, 0xd0, 0xd0);    // silver
  export const GrayscaleColor91 = GrayscaleColor.convertToGrayscale(Color91);

  export const Color92 = new RGBColor(0xa0, 0xa0, 0xa0);    // gray
  export const GrayscaleColor92 = GrayscaleColor.convertToGrayscale(Color92);

  export const Color93 = new RGBColor(0x60, 0x60, 0x60);    // dark gray
  export const GrayscaleColor93 = GrayscaleColor.convertToGrayscale(Color93);


  export const Color99 = new RGBColor(0, 0, 0);
  export const GrayscaleColor99 = GrayscaleColor.convertToGrayscale(Color99);


  // White/Black excluded.
  export const Colors = [
    // Put BW colrs in front. This will not change when add/subtract colors.
    // A number of color tag constants uses the indicex, 0 through 3.
    // So, don't change this...
    Color90, Color91, Color92, Color93,
    // Color01, Color02, Color03, Color04, Color05, Color06, 
    // Color07, Color08, Color09, Color10, Color11, Color12,
    // Color13, Color14, Color15, Color16, Color17, Color18
    Color01, Color04,    // R
    Color13, Color17,    // R/bg
    Color02, Color05,    // G
    Color15, Color16,    // G/br
    Color03, Color06,    // B
    Color14, Color18,    // B/rg
    Color07, Color10,    // RG
    Color08, Color11,    // BR
    Color09, Color12     // GB
  ];
  export const Size = Colors.length;

  // Same indices as Colors.
  export const GrayscaleColors = [
    GrayscaleColor90, GrayscaleColor91, GrayscaleColor92, GrayscaleColor93,
    // GrayscaleColor01, GrayscaleColor02, GrayscaleColor03, GrayscaleColor04, GrayscaleColor05, GrayscaleColor06, 
    // GrayscaleColor07, GrayscaleColor08, GrayscaleColor09, GrayscaleColor10, GrayscaleColor11, GrayscaleColor12,
    // GrayscaleColor13, GrayscaleColor14, GrayscaleColor15, GrayscaleColor16, GrayscaleColor17, GrayscaleColor18
    GrayscaleColor01, GrayscaleColor04,    // R
    GrayscaleColor13, GrayscaleColor17,    // R/bg
    GrayscaleColor02, GrayscaleColor05,    // G
    GrayscaleColor15, GrayscaleColor16,    // G/br
    GrayscaleColor03, GrayscaleColor06,    // B
    GrayscaleColor14, GrayscaleColor18,    // B/rg
    GrayscaleColor07, GrayscaleColor10,    // RG
    GrayscaleColor08, GrayscaleColor11,    // BR
    GrayscaleColor09, GrayscaleColor12     // GB
  ];

  // export const GrayscaleMap = {
  //   Color00: GrayscaleColor00,
  //   // ...
  // };


  // TBD:Validate indexes?
  export function convertToColors(indexes: number[]): RGBColor[] {
    let colours: RGBColor[] = [];
    if (indexes) {
      // in vs of?
      for (let idx of indexes) {
        colours.push(Colors[idx]);
      }
      // let len = indexes.length;
      // for (let i=0; i<len; i++) {
      //   let idx = indexes[i];
      //   colours.push(Colors[idx]);
      // }
      // if(isDL()) dl.log('>>>>>>>>>>>>>> len = ' + len + '; colours.length = ' + colours.length);
    }
    return colours;
  }

  export function convertToGrayscaleColors(indexes: number[]): GrayscaleColor[] {
    let colours: GrayscaleColor[] = [];
    if (indexes) {
      for (let idx of indexes) {
        colours.push(GrayscaleColors[idx]);
      }
    }
    return colours;
  }
  

  // Returns an array of two arrays:
  // First array: Closely related colors.
  // Scoend array: Somewhat related colors.
  // The rest is considred neutral or different.
  // This is dependent on Colors array.
  // If it changes, then this function needs to be updated as well.
  // TBD: Need to make this more robust... 
  export function similarColorIndexes(colorIndex: number): [number[], number[]] {
    let c1: number[] = [];
    let c2: number[] = [];

    // Note: we are exluding self from c1/c2.
    // tbd: Is using hash map more effiicent than using switch() ?????
    switch (colorIndex) {
      case 0: case 1: case 2: case 3:
        c1.push(0, 1, 2, 3);
        c2.push(4, 5, 8, 9, 12, 13);  // Pure colors only.
        break;

      case 4:
        c1.push(5, 6, 7);
        c2.push(16, 17, 18, 19);
        break;
      case 5:
        c1.push(4, 6, 7);
        c2.push(16, 17, 18, 19);
        break;
      case 6:
        c1.push(4, 5, 7);
        c2.push(16, 17, 18, 19);
        break;
      case 7:
        c1.push(4, 5, 6);
        c2.push(16, 17, 18, 19);
        break;

      case 8:
        c1.push(9, 10, 11);
        c2.push(16, 17, 20, 21);
        break;
      case 9:
        c1.push(8, 10, 11);
        c2.push(16, 17, 20, 21);
        break;
      case 10:
        c1.push(8, 9, 11);
        c2.push(16, 17, 20, 21);
        break;
      case 11:
        c1.push(8, 9, 10);
        c2.push(16, 17, 20, 21);
        break;

      case 12:
        c1.push(13, 14, 15);
        c2.push(18, 19, 20, 21);
        break;
      case 13:
        c1.push(12, 14, 15);
        c2.push(18, 19, 20, 21);
        break;
      case 14:
        c1.push(12, 13, 15);
        c2.push(18, 19, 20, 21);
        break;
      case 15:
        c1.push(12, 13, 14);
        c2.push(18, 19, 20, 21);
        break;

      case 16:
        c1.push(17, 6, 7, 10, 11);
        c2.push(4, 5, 8, 9);
        break;
      case 17:
        c1.push(16, 6, 7, 10, 11);
        c2.push(4, 5, 8, 9);
        break;

      case 18:
        c1.push(19, 6, 7, 14, 15);
        c2.push(4, 5, 12, 13);
        break;
      case 19:
        c1.push(18, 6, 7, 14, 15);
        c2.push(4, 5, 12, 13);
        break;

      case 20:
        c1.push(21, 10, 11, 14, 15);
        c2.push(8, 9, 12, 13);
        break;
      case 21:
        c1.push(20, 10, 11, 14, 15);
        c2.push(8, 9, 12, 13);
        break;
    }

    return [c1, c2];
  }

  export function randomWeightedIndex(pivotColorIndex: number, excluded: (number[] | null) = null): number {
    // Compute similar colors
    let [c1, c2] = similarColorIndexes(pivotColorIndex);

    // Divide it into three bins.
    // Note that the "rest" (bin3) contains all colors,
    // hence the c1, c2 weights are extra above the equal weight.
    // let bin1 = Math.floor(c1.length * 1.5);
    // let bin2 = Math.floor(c2.length * 1);
    let bin1 = c1.length * 3;
    let bin2 = c2.length * 2;
    // let bin3 = Size - c1.length - c2.length;
    let bin3 = Size;
    let W = bin1 + bin2 + bin3;

    let idx = pickWeightedIndex(c1, c2, W, bin1, bin2, bin3);
    if (excluded && excluded.length > 0) {
      let cnt = 0;
      while ((cnt++ < 10) && (excluded.indexOf(idx) > -1)) {   // max 10 tries.
        idx = pickWeightedIndex(c1, c2, W, bin1, bin2, bin3);
      }
    }
    return idx;
  }
  function pickWeightedIndex(c1: number[], c2: number[],  W: number, bin1: number, bin2: number, bin3: number): number {
    // assert W = bin1 + bin2 + bin3.
    let idx: number = 0;
    let b = Math.floor(Math.random() * W);
    if (b < bin1) {
      idx = c1[Math.floor(Math.random() * c1.length)];
    } else if (b < bin2) {
      idx = c2[Math.floor(Math.random() * c2.length)];
    } else {
      idx = Math.floor(Math.random() * Size);
    }
    return idx;
  }


  // Note:
  // Used on to seed the center color.
  // The rest 8 colors are picked by randomWeightedIndex()
  export function randomIndex(excluded: (number[] | null) = null): number {
    // let idx = Size - 1;

    let idx = Math.floor(Math.random() * Size);
    if (excluded && excluded.length > 0) {
      let cnt = 0;
      while ((cnt++ < 10) && (excluded.indexOf(idx) > -1)) {   // max 10 tries.
        idx = Math.floor(Math.random() * Size);
      }
    }
    return idx;
  }

  export function randomColor(): RGBColor {
    let idx = Math.floor(Math.random() * Size);
    return Colors[idx];
  }

}
