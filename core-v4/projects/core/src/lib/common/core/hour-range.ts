import { DualValue } from './dual-value';

// tbd: range vs {start time, duration} ????


/**
 * Represents a time in a day. (E.g, 15.5 -> 2:30PM, 26 -> 2AM next day, etc.)
 * "Hour" really represents a (one hour) time slot, rather than a point in time.
 */
// TBD: hour + interval ?
export class HourRange implements DualValue 
{
  // // DualValue interface.
  // get lower(): number {
  //   return this.startHour;
  // }
  // set lower(_lower: number) {
  //   this.startHour = _lower;
  // }
  // get upper(): number {
  //   return this.endHour;
  // }
  // set upper(_upper: number) {
  //   this.endHour = _upper;
  // }

  // // [ : )
  // // Uess 24 hour format with number > 24 indicating the next day.
  // // endHour == -1 means it has only start time not end time.
  // constructor(public startHour: number = 0, public endHour: number = 0) {
  //   // TBD:
  //   // valid range? e.g., startHour <= endHour.
  // }

  // DualValue interface.
  get startHour(): number {
    return this.lower;
  }
  set startHour(_startHour: number) {
    this.lower = _startHour;
  }
  get endHour(): number {
    return this.upper;
  }
  set endHour(_endHour: number) {
    this.upper = _endHour;
  }

  // [ : )
  // Uess 24 hour format with number > 24 indicating the next day.
  // endHour == -1 means it has only start time not end time.
  constructor(public lower: number = 0, public upper: number = 0) {
    // TBD:
    // validate range? e.g., startHour <= endHour.
  }

  // isInside(hour: number): boolean {
  //   return (hour >= this.startHour && hour < this.endHour);
  // }
  // isOutside(hour: number): boolean {
  //   return (hour < this.startHour || hour >= this.endHour);
  // }

  isInside(hour: number): boolean {
    return (hour >= this.lower && (this.upper === -1 || hour < this.upper));
  }
  isOutside(hour: number): boolean {
    return (hour < this.lower || (this.upper !== -1 && hour >= this.upper));
  }


  toString(): string {
    return '(' + this.startHour + ':' + this.endHour + ')';
  }

  clone(): HourRange {
    let cloned = Object.assign(new HourRange(), this) as HourRange;
    return cloned;
  }
  static clone(obj: any): HourRange {
    let cloned = Object.assign(new HourRange(), obj) as HourRange;
    return cloned;
  }

}
