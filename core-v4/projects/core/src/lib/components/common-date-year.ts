import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';

import { DevLogger as dl } from '../common/logging/dev-logger'; import isDL = dl.isLoggable;
import { DateTimeUtil } from '../common/util/date-time-util';


/**
 * Component for displaying the current year only.
 */
@Component({
  selector: 'common-date-year',
  template: `
    <span>{{year}}</span>
`
})
export class CommonDateYearComponent {

  @Input("date") date: Date;
  year: number = 0;

  constructor(
  ) {
    this.date = new Date();
  }

  ngOnInit() {
    // if(isDL()) dl.log(">>>>>> date = " + this.date.toLocaleDateString());
    this.year = DateTimeUtil.getYear(this.date);
    // if(isDL()) dl.log(">>>>>> year = " + this.year);
  }

}
