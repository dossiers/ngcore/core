# @ngcore/core
> NG Core angular/typescript core library


Core classes library for Angular.
(Current version requires Angular v7.2+)


## API Docs

* http://ngcore.gitlab.io/core/


